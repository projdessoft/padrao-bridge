package br.ufrn.imd.padrao.bridge.device;

public interface DeviceImplementation {
    String getName();

    void setName(String name);

    boolean getPower();

    void setPower(boolean power);

    int getVolume();

    void setVolume(int volume);
}
