package br.ufrn.imd.padrao.bridge.main;

import br.ufrn.imd.padrao.bridge.device.Radio;
import br.ufrn.imd.padrao.bridge.device.TV;
import br.ufrn.imd.padrao.bridge.remotecontrol.RemoteControl;
import br.ufrn.imd.padrao.bridge.remotecontrol.RemoteControlAbstraction;
import br.ufrn.imd.padrao.bridge.remotecontrol.RemoteControlWithVolume;

public class Main {
    public static void clientCode(RemoteControlAbstraction abstracao) {
        abstracao.togglePower();

        if ( abstracao instanceof RemoteControlWithVolume) {
            ((RemoteControlWithVolume) abstracao).volumeUp(); // 15
            ((RemoteControlWithVolume) abstracao).volumeDown(); // 10
            ((RemoteControlWithVolume) abstracao).volumeDown(); // 5
        }
    }
    public static void main(String[] args) {
        TV tv = new TV();
        Radio radio = new Radio();

        RemoteControl remoteControlRadio = new RemoteControl(radio);
        clientCode(remoteControlRadio);

        RemoteControlWithVolume remoteControlWithVolumeTV = new RemoteControlWithVolume(tv);
        clientCode(remoteControlWithVolumeTV);


    }
}
