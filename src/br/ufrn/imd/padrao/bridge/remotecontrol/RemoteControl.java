package br.ufrn.imd.padrao.bridge.remotecontrol;

import br.ufrn.imd.padrao.bridge.device.DeviceImplementation;

public class RemoteControl extends RemoteControlAbstraction {
    public RemoteControl(DeviceImplementation deviceImplementation) {
        super(deviceImplementation);
    }
}
