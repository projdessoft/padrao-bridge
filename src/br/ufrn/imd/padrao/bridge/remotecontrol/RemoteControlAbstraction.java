package br.ufrn.imd.padrao.bridge.remotecontrol;

import br.ufrn.imd.padrao.bridge.device.DeviceImplementation;

public abstract class RemoteControlAbstraction {

    protected DeviceImplementation device;

    public RemoteControlAbstraction(DeviceImplementation deviceImplementation) {
        this.device = deviceImplementation;
    }

    public void togglePower() {
        this.device.setPower(!this.device.getPower());
        System.out.println(this.device.getName() + " power status " + this.device.getPower());
    }
}
