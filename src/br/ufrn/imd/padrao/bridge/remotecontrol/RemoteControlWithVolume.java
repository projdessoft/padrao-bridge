package br.ufrn.imd.padrao.bridge.remotecontrol;

import br.ufrn.imd.padrao.bridge.device.DeviceImplementation;

public class RemoteControlWithVolume extends RemoteControlAbstraction {
    public RemoteControlWithVolume(DeviceImplementation deviceImplementation) {
        super(deviceImplementation);
    }

    public void volumeUp() {
        int oldVolume = this.device.getVolume();
        this.device.setVolume(this.device.getVolume() + 5);
        System.out.println(this.device.getName() + " tinha o volume " + oldVolume + " agora tem " + this.device.getVolume());
    }

    public  void volumeDown() {
        int oldVolume = this.device.getVolume();
        this.device.setVolume(this.device.getVolume() - 5);
        System.out.println(this.device.getName() + " tinha o volume " + oldVolume + " agora tem " + this.device.getVolume());
    }
}
